const webpack = require('webpack'),
    path = require('path'),
    ExtractTextPlugin = require("extract-text-webpack-plugin"),
    glob = require('glob');


let entries = getEntry('./src/pages/**/index.js');//获取文件入口
module.exports = {
    entry: entries,
    output: {
        path: path.resolve(__dirname, 'build'), // html,css,js,图片等资源文件的输出路径，将所有资源文件放在dist目录
        publicPath: '../',                  // html,css,js,图片等资源文件在server上的存放路径
        filename: '[name].js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,//js结尾
                exclude: /node_module/,//排除node_module目录下面的js文件
                loader: 'babel',//ES6转换为ES5
                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.(png|jpg|gif)$/,//图片格式为png或jpg或gif
                loader: 'url-loader?limit=10240'//这里的 limit=10240 表示用 base64 编码 <= 10K 的图像
            }, {
                //sass编译成css，需要style-loader、css-loader、sass-loader和node-sass四个npm包
                //node-sass:用于处理scss文件中通过@import导入的文件
                //scss编译成css，并压缩css代码
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader")//打包后样式是css文件，通过<link>标签引入
            }, {
                test: /\.css/,
                loader: 'style!css'//打包后样式放在<style>标签内
            }
        ]
    },
    plugins: [
        // 配置提取出的样式文件，使用link引用
        new ExtractTextPlugin('[name].css'),
        //配置打包环境（production/development）
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"development"'
        }),
        //热加载
        new webpack.HotModuleReplacementPlugin()
    ],
    //设置代理，解决本地服务器提交ajax请求跨域问题
    devServer: {
        //请求'/api/feeds'转向请求'http://snowx.codoon.com/api/feeds'
        // proxy: {
        //     '/api/feeds': {
        //         target: 'http://snowx.codoon.com',
        //         secure: false,
        //         changeOrigin: true
        //     }
        // }
    }
};

var prod = process.env.NODE_ENV === 'production';
module.exports.plugins = (module.exports.plugins || []);
if (prod) {
    module.exports.devtool = 'src-map';
    module.exports.plugins = module.exports.plugins.concat([
        //js代码压缩
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        //压缩文件
        new webpack.optimize.OccurenceOrderPlugin()
    ]);
} else {
    module.exports.devtool = 'eval-src-map';
    module.exports.output.publicPath = '/build/';//热加载时，publicPath要指向webpack-dev-server服务器目录，不然实时更新没有效果哦
}

// 根据项目具体需求，输出正确的js、css和html路径
function getEntry(globPath) {
    var entries = {},
        basename, tmp, pathname;
    glob.sync(globPath).forEach(function (entry) {
        basename = path.basename(entry, path.extname(entry));
        tmp = entry.split('/').splice(-3);
        pathname = tmp.splice(1, 1) + '/' + basename; // 正确输出js和html的路径
        entries[pathname] = entry;
    });
    return entries;
}