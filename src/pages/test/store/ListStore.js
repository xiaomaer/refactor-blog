/**
 * Created by xiaoma on 2016/12/20.
 */
import { ReduceStore } from 'flux/utils';
import AppDispatcher from './../../../common/dispatcher/dispatcher';
class ListStore extends ReduceStore {
  constructor() {
    super(AppDispatcher);
  }
  getInitialState() {
    return [];
  }

  reduce(state, action) {
    switch (action.actionType) {
      case 'ADD_NEW_ITEM':
        var newState=state.slice();
        newState.push(action.text);
        return newState;
      default:
        return state;
    }
  }
}

export default new ListStore();