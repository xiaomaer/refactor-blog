/**
 * Created by xiaoma on 2016/12/19.
 */
import './index.scss';
import React from 'react';
import {render} from 'react-dom';
import  MyButtonController from './view/MyButtonController';

render(<MyButtonController/>,document.querySelector('#example'));