import AppDispatcher from './../../../common/dispatcher/dispatcher';
var ButtonActions = {
    addNewItem: function (text) {
        AppDispatcher.dispatch({
            actionType: 'ADD_NEW_ITEM',
            text: text
        });
    }
};
export default ButtonActions