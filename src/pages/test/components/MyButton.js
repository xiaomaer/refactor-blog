import React, {Component} from 'react';

export default class MyButton extends Component {
    render(){
        let html = this.props.items.map(function (item,index) {
            return <li data-key={index} className="list" key={index}>{item+index}</li>;
        });
        return (
            <div className="container">
                <ul className="lists">{html}</ul>
                <button type="button" onClick={this.props.onClick} className="btn">add item</button>
            </div>
        )
    }
}