import React, { Component } from 'react';
import { Container } from 'flux/utils'
import ButtonActions from '../action/ButtonActions';
import ListStore from '../store/ListStore';
import MyButton from '../components/MyButton';
import Picker from './../../../common/components/picker/picker'

class MyButtonController extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: ListStore.getState(),
			show: false
		}
		this.createNewItem = this.createNewItem.bind(this);
		this.hidePicker = this.hidePicker.bind(this);
		this.showPicker = this.showPicker.bind(this);
		this.changeItem = this.changeItem.bind(this);
	}
	static getStores() {
		return [ListStore]
	}
	static calculateState() {
		return {
			items: ListStore.getState()
		}
	}
	createNewItem() {
		ButtonActions.addNewItem('new item');
	}
	hidePicker() {
		this.setState({
			show: false
		})
	}
	showPicker() {
		this.setState({
			show: true
		})
	}
	changeItem(item, index) {
		this.hidePicker();
		console.log('您选中了第' + (index + 1) + '项，值为：' + item);
	}
	render() {
		const items = ['选项1', '选项2', '选项3', '选项4', '选项5', '选项6', '选项7'];
		return (
			<div>
				<MyButton onClick={this.createNewItem} items={this.state.items} />
				<button type="button" onClick={this.showPicker}>单列选择器</button>
				<Picker items={items} show={this.state.show} onCancel={this.hidePicker} onChange={this.changeItem} defaultIndex={1}/>
			</div>
		);
	}
}
export default Container.create(MyButtonController);