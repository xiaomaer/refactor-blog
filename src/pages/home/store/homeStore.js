/**
 * Created by xiaoma on 2016/12/21.
 */
import {ReduceStore} from 'flux/utils'
import AppDispatcher from './../../../common/dispatcher/dispatcher';
class HomeStore extends ReduceStore {
    constructor() {
        super(AppDispatcher);
    }
    getInitialState() {
        return {
            bigTypes: [],
            articles: {},
            newArticles: [],
            hotArticles: [],
            articleDetail: {
                article: {},
                catName: "",
                prev: null,
                next: null
            }
        };
    }
    reduce(state, action) {
        switch (action.actionType) {
            case 'ARTICLE_TYPE':
                var newState = Object.assign({}, state),
                    data = action.data,
                    bigTypes = data[0];
                for (let i = 0, blen = bigTypes.length; i < blen; i++) {
                    let ID = bigTypes[i].ID;
                    for (let j = 1, len = data.length; j < len; j++) {
                        if (data[j][0].ParentID === ID) {
                            bigTypes[i].subTypes = data[j];
                            break;
                        }
                    }
                }
                newState.bigTypes = bigTypes;
                return newState;
            case 'ARTICLE_LIST':
                var data = action.data,
                    newState = Object.assign({}, state, {articles: data});
                newState.articles.page_total = Math.ceil(data.total / 20);
                return newState;
            case 'NEW_ARTICLE_LIST':
                var newState = Object.assign({}, state, {newArticles: action.data});
                return newState;
            case 'HOT_ARTICLE_LIST':
                var newState = Object.assign({}, state, {hotArticles: action.data});
                return newState;
            case 'ARTICLE_DETAIL':
                var newState = Object.assign({}, state, {articleDetail: action.data});
                return newState;
            default:
                return state;
        }
    }
}
export default new HomeStore();
