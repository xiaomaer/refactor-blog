/**
 * Created by xiaoma on 2016/12/21.
 */
import AppDispatcher from './../../../common/dispatcher/dispatcher';
let PageActions = {
     //获取文章类型
    getArticleTypes: function (data) {
        AppDispatcher.dispatch({
            actionType: 'ARTICLE_TYPE',
            data: data
        })
    },
    //获取文章列表
    getArticleList: function (data) {
        AppDispatcher.dispatch({
            actionType: 'ARTICLE_LIST',
            data: data
        })
    },
    //获取最新文章列表
    getNewArticleList: function (data) {
        AppDispatcher.dispatch({
            actionType: 'NEW_ARTICLE_LIST',
            data: data
        })
    },
    //获取最热文章列表
    getHotArticleList:function(data){
        AppDispatcher.dispatch({
            actionType:'HOT_ARTICLE_LIST',
            data:data
        })
    },
    //获取文章详情
    getArticleDetail:function(data){
        AppDispatcher.dispatch({
            actionType:'ARTICLE_DETAIL',
            data:data
        })
    }
};
export default PageActions;
