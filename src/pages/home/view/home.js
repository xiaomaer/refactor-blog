import React, {Component} from 'react';
import {Container} from 'flux/utils';
import homeStore from '../store/HomeStore';
import Nav from '../components/nav';
import TypeList from '../components/typeList';
import DateList from '../components/dateList';
import ArticleList from '../components/articleList';
import Tags from '../components/tags';
import Articles from '../components/articles';
import Pager from '../components/pager';
import API from '../api/homeAPI';

import Detail from './../components/detail';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: homeStore.getState(),
            page_num: 1,
            cid: 0,
            query: "",
            type_name: '全部文章'
        }
        this.bindFunctions();
    }
    static getStores() {
        return [homeStore]
    }
    static calculateState() {
        return {
            info: homeStore.getState()
        }
    }
    bindFunctions() {
        this.typeArticles = this
            .typeArticles
            .bind(this);
        this.prevPage = this
            .prevPage
            .bind(this);
        this.nextPage = this
            .nextPage
            .bind(this);
        this.changePage = this
            .changePage
            .bind(this);
    }
    // 上一页
    prevPage(e) {
        e.stopPropagation();
        const {cid, query, type_name, page_num} = this.state;
        if (page_num === 1) 
            return;
        let curr_page = page_num - 1;
        this.setState({
            page_num: curr_page
        }, () => {
            API.getArticleList(cid, query, curr_page, type_name);
        });
    }
    // 下一页
    nextPage(e) {
        e.stopPropagation();
        const {info,cid, query, type_name, page_num} = this.state;
        if (page_num === info.articles.page_total) 
            return;
        let curr_page = page_num + 1;
        this.setState({
            page_num: curr_page
        }, () => {
            API.getArticleList(cid, query, curr_page, type_name);
        });
    }
    // 切换到指定页
    changePage(e) {
        let curr_page = e.target.dataset['id']
            ? parseInt(e.target.dataset['id'])
            : 0;
        if (curr_page !== 0) {
            const {cid, query, type_name} = this.state;
            this.setState({
                page_num: curr_page
            }, () => {
                API.getArticleList(cid, query, curr_page, type_name);
            });
        }
    }
    // 根据类型获取文章列表
    typeArticles(typeID, queryStr, typeName) {
        this.setState({
            page_num: 1,
            cid: typeID,
            query: queryStr,
            type_name: typeName
        }, () => {
            API.getArticleList(typeID, queryStr, 1, typeName)
        });
    }

    render() {
        const {info, page_num} = this.state;
        return (
            <div>
                <Nav types={info.bigTypes}/>
                <div className="header_fix"></div>
                <div className="banner">
                    <span className="banner_logo page_container">只希望每次的记录都有些许突破～～</span>
                </div>
                <div className="content page_container">
                    <div className="side_left">
                        {/* 文章类型分类*/}
                        <TypeList title="分类目录" types={info.bigTypes} typeArticles={this.typeArticles}/> {/* 文章发布时间分类*/}
                        {/*<DateList title="文章归档"/>*/}
                    </div>
                    <div className="main">
                        {/*首页*/}
                        <Articles data={info.articles}/> {info.articles.page_total
                            ? (<Pager
                                pageTotal={info.articles.page_total}
                                currNum={page_num}
                                nextPage={this.nextPage}
                                prevPage={this.prevPage}
                                changePage={this.changePage}/>)
                            : ''}
                    </div>
                    <div className="side_right">
                        <ArticleList
                            title="最新文章"
                            icon="icon-zuixin1"
                            data={info.newArticles}
                            type="new"/>
                        <ArticleList
                            title="最热文章"
                            icon="icon-remen-copy"
                            data={info.hotArticles}
                            type="hot"/> {/* <Tags title="标签" icon="icon-biaoqian"/>*/}
                    </div>
                </div>
            </div>
        );
    }
}
export default Container.create(Home);