/**
 * Created by xiaoma on 2016/12/21.
 */
import pageActions from '../action/homeActions';

class API {
    /**
     * 获取文章类型
     * @param {*} userID ：用户ID，默认0，1:他，2:我
     */
    getArticleAllTypes(userID) {
        let types = JSON.parse(sessionStorage.getItem('types'));
        if (types) {
            pageActions.getArticleTypes(types);
            return;
        }
        $.ajax({
            type: 'get',
            url: 'http://dobit.top/Api/Category',
            data: {
                userID: userID //0:全部，1:他，2:我
            },
            success: function (data) {
                sessionStorage.setItem('types', JSON.stringify(data));
                pageActions.getArticleTypes(data);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    /**
     *获取文章列表
     * @param {*} cid ：int,分类ID,为0表示不指定分类，获取所有的文章
     * @param {*} cid ：string, 查询字符串
     * @param {*} page_num ：int,当前页码
     * @param {*} type_name ：string,文章类型
     */
    getArticleList(cid, query, page_num, type_name) {
        const typeName = type_name
            ? type_name
            : '全部文章';
        $.ajax({
            type: "get",
            url: "http://dobit.top/Api/Article",
            data: {
                userID: 2, //int,用户ID
                cid: cid,
                query:query,
                page: page_num,
                size: 20 //int,每页个数
            },
            success: function (data) {
                data.type_name = typeName;
                pageActions.getArticleList(data);
                //最新文章列表
                if (cid === 0 && page_num === 1) {
                    let rows = data.rows.slice(0, 10);
                    pageActions.getNewArticleList(rows);
                }
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
    /**
     * 获取热门文章列表
     */
    getHotArticleList(){
         $.ajax({
            type: "get",
            url: "http://dobit.top/Api/Hot",
            data: {
                userID: 2 //int,用户ID
            },
            success: function (data) {
                pageActions.getHotArticleList(data.slice(0, 10));
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
    /**
     * 用来获取文章详细信息及上一篇和下一篇文章简介
     * @param {*} ID :文章ID
     */
    getArticleDetail(ID) {
        $.ajax({
            type: 'get',
            url: 'http://dobit.top/Api/Article',
            data: {
                userID: 2,
                id: ID
            },
            success: function (data) {
                pageActions.getArticleDetail(data);
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
}
export default new API();