/**
 * Created by xiaoma on 2016/12/20.
 */
import './index.scss'
import React, {Component} from 'react';
import {render} from 'react-dom';
import Page from './view/home';
import API from './api/homeAPI';
render(
    <Page/>, document.querySelector('#J-container'));
//获取文章类型列表
API.getArticleAllTypes(2);
if (getUrlParams('CatID')) {
    const CatID = parseInt(getUrlParams('CatID')),
        CatName = getUrlParams('CatName');
    API.getArticleList(CatID, "", 1, CatName);
} else {
    //获取全部文章列表
    API.getArticleList(0, "", 1);
}
//获取最热文章列表
API.getHotArticleList();
/* API.getArticleDetail(343);*/

// 获取查询字符串
function getUrlParams(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"),
        r = window
            .location
            .search
            .substr(1)
            .match(reg);
    if (r != null) 
        return decodeURIComponent(r[2]);
    return null;
}