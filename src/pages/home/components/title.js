/*左右侧边栏标题*/
import React, { Component } from 'react';

export default class Title extends Component {
    render() {
        const { title, icon } = this.props;
        return (
            <div className="title">
                <h1 className="title_name">
                    {icon ? (<i className={'iconfont ' + icon}></i>) : ''}
                    {title}
                </h1>
            </div>
        )
    }
}