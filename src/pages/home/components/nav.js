/*顶部导航条*/
/**
 * Created by xiaoma on 2016/12/20.
 */
import React, { Component } from 'react';

export default class Nav extends Component {
    render() {
        const {types}=this.props;
        const menus=types.map((item,index)=>{
            return(
                 <li className="nav_submenu" key={index}><a href={"home.html?CatID="+item.ID+"&CatName="+item.CatName} className="nav_sublink">{item.CatName}</a></li>
            )
        });
        return (
            <header>
                {/*pc端导航条*/}
                <div className="header header_bg_white only_pc_show">
                    <div className="nav_PC page_container">
                        <div className="nav_logo">XIAOMAER</div>
                        <ul className="nav_menus">
                            <li className="nav_menu"><a href="home.html" className="nav_link">首页</a></li>
                            <li className="nav_menu">
                                <a href="#" className="nav_link">文章类型</a>
                                <ul className="nav_submenus">
                                    {menus}
                                    {/*<li className="nav_submenu"><a href="#" className="nav_sublink">前端开发</a></li>
                                    <li className="nav_submenu"><a href="#" className="nav_sublink">后端开发</a></li>
                                    <li className="nav_submenu"><a href="#" className="nav_sublink">移动开发</a></li>
                                    <li className="nav_submenu"><a href="#" className="nav_sublink">其他</a></li>*/}
                                </ul>
                            </li>
                            <li className="nav_menu"><a href="#" className="nav_link">前端资源库</a></li>
                            <li className="nav_menu"><a href="#" className="nav_link">关于我</a></li>
                        </ul>
                        <div className="nav_search">
                            <i className="iconfont icon-search"></i>
                        </div>
                    </div>
                </div>
                {/*移动端导航条*/}
                <div className="header header_bg_black only_mobile_show">
                    <div className="nav_mobile page_container">
                        <div className="nav_categories">
                            <i className="iconfont icon-fenlei"></i>
                        </div>
                        <div className="nav_logo">XIAOMAER</div>
                        <div className="nav_search">
                            <i className="iconfont icon-search"></i>
                        </div>
                    </div>
                    <ul className="nav_menus_left">
                        <li className="nav_menu"><a href="#" className="nav_link">首页</a></li>
                        <li className="nav_menu">
                            <a href="#" className="nav_link">文章类型</a>
                            <ul className="nav_submenus">
                                {menus}
                                {/*<li className="nav_submenu"><a href="#" className="nav_sublink">前端开发</a></li>
                                <li className="nav_submenu"><a href="#" className="nav_sublink">后端开发</a></li>
                                <li className="nav_submenu"><a href="#" className="nav_sublink">移动开发</a></li>
                                <li className="nav_submenu"><a href="#" className="nav_sublink">其他</a></li>*/}
                            </ul>
                        </li>
                        <li className="nav_menu"><a href="#" className="nav_link">关于我</a></li>
                    </ul>
                </div>
            </header>
        )
    }
}

