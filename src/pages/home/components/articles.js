/*中间文章列表*/
import React, { Component } from 'react';
import LazyLoad from 'react-lazy-load';

export default class Articles extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { data } = this.props;
        const articles = data.rows && data.rows.map((item, index) => {
            return (
                <li className="articles_item" key={index}>
                    <a href={"detail.html?id=" + item.ID} className="articles_img_container">
                        <LazyLoad>
                            <img src={item.MainImage?('http://7xj89g.com1.z0.glb.clouddn.com/'+item.MainImage):'http://demo.shejidaren.com/dr-ii/wp-content/themes/dr-ii/static/images/min/frontend/thumb-preview.png'} className="articles_img" />
                        </LazyLoad>
                    </a>
                    <div className="articles_info">
                        <div className="articles_describe">
                            <a href={"detail.html?id=" + item.ID} className="articles_name">{item.Title}</a>
                            <div className="articles_content">{item.Description || '等你到了一定的年龄，彼此的过去已经不再重要，那些曾经令你无比在乎的东西就好像半途而退的潮汐一般似乎没有了提及的'}</div>
                        </div>
                        <div className="articles_tags">
                            <a href={"detail.html?id=" + item.ID} className="artticles_read">阅读全文</a>
                            <span className="articles_tag"><i className="iconfont icon-dateto"></i><span>{item.CreateDateTime.substr(0,10)}</span></span>
                            <span className="articles_tag"><i className="iconfont icon-liulan"></i><span>{item.Clicks}</span></span>
                            <span className="articles_tag"><i className="iconfont icon-xinxi"></i><span id = {"sourceId::"+item.ID} className = "cy_cmt_count" >18</span></span>
                        </div>
                    </div>
                </li>
            )
        });
        return (
            <div className="articles">
                <h1 className="articles_type">{data.type_name}</h1>
                <ul>
                    {articles}
                    {/*<li className="articles_item">
                        <a href="#" className="articles_img_container">
                            <img src="http://demo.shejidaren.com/dr-ii/wp-content/uploads/2015/01/78D1F7952121-210x140.jpg" className="articles_img" />
                        </a>
                        <div className="articles_info">
                            <a href="#" className="articles_name">不属于我的东西，我不要。不是真心给我的东西，我不要</a>
                            <div className="articles_content">等你到了一定的年龄，彼此的过去已经不再重要，那些曾经令你无比在乎的东西就好像半途而退的潮汐一般似乎没有了提及的</div>
                            <div className="articles_tags">
                                <a href="#" className="artticles_read">阅读全文</a>
                                <span className="articles_tag"><i className="iconfont icon-dateto"></i><span>2016.01.09</span></span>
                                <span className="articles_tag"><i className="iconfont icon-liulan"></i><span>1468</span></span>
                                <span className="articles_tag"><i className="iconfont icon-xinxi"></i><span>18</span></span>
                            </div>
                        </div>
                    </li>*/}
                </ul>
            </div>
        )
    }
}