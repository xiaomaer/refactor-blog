/*左侧标签列表*/
import React, { Component } from 'react';
import Title from './title';

export default class Tags extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { title, icon } = this.props;
        return (
            <div className="tag">
                <Title title={title} icon={icon} />
                <ul>
                    <li className="tag_item">flex</li>
                    <li className="tag_item">gird</li>
                    <li className="tag_item">box-shadow</li>
                    <li className="tag_item">垂直居中</li>
                    <li className="tag_item">左右布局</li>
                    <li className="tag_item">css3</li>
                    <li className="tag_item">html5</li>
                    <li className="tag_item">选择器</li>
                    <li className="tag_item">模版</li>
                    <li className="tag_item">webpack</li>
                </ul>
            </div>
        )
    }
}