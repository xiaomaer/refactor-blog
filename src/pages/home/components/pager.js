/*文章列表分页*/
import React, { Component } from 'react';

export default class Pager extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {pageTotal,currNum,prevPage,nextPage,changePage} = this.props;
        let item=[];
        if (pageTotal <= 5) {
            for (let i = 1; i <= pageTotal; i++) {
                if (i === currNum) {
                    item.push(<li className="pager_item pager_item_active" key={'page_' + i} data-id={i}>{i}</li>);
                } else {
                    item.push(<li className="pager_item" key={'page_' + i} data-id={i}>{i}</li>);
                }
            }
        } else {
            if (currNum < 5) {
                for (let i = 1; i <= 5; i++) {
                    if (i === currNum) {
                        item.push(<li className="pager_item pager_item_active" key={'page_' + i} data-id={i}>{i}</li>);
                    } else {
                        item.push(<li className="pager_item" key={'page_' + i} data-id={i}>{i}</li>);
                    }
                }
                item.push(<li className="ellipsis" key="last">...</li>);
            } else if (currNum > (pageTotal - 3)) {
                item.push(<li className="ellipsis" key="first">...</li>);
                for (let i = (pageTotal - 4); i <= pageTotal; i++) {
                    if (i === currNum) {
                        item.push(<li className="pager_item pager_item_active" key={'page_' + i} data-id={i}>{i}</li>);
                    } else {
                        item.push(<li className="pager_item" key={'page_' + i} data-id={i}>{i}</li>);
                    }
                }
            } else {
                item.push(<li className="ellipsis" key="first">...</li>);
                for (let i = (currNum - 2); i <= (currNum + 2); i++) {
                    if (i === currNum) {
                        item.push(<li className="pager_item pager_item_active" key={'page_' + i} data-id={i}>{i}</li>);
                    } else {
                        item.push(<li className="pager_item" key={'page_' + i} data-id={i}>{i}</li>);
                    }
                }
                item.push(<li className="ellipsis" key="last">...</li>);
            }
        }

        return (
            <div className="pager">
                <button className="pager_button only_mobile_show" onClick={nextPage}>查看更多文章</button>
                <ul className="pager_items" onClick={changePage}>
                    <li className={'pager_item'+(currNum===1?' pager_item_disabled':'')} onClick={prevPage}>《</li>
                    {item}
                    <li className={'pager_item'+(currNum===pageTotal?' pager_item_disabled':'')} onClick={nextPage}>》</li>
                </ul>
            </div>
        )
    }
}