import React,{Component} from 'react';

export default class SubMenus extends Component{
    render(){
        const data = this.props.data;
        const submenus = data.map((item, index) => {
           return (
                <li className="subtype_item" key={"s_" + index}>
                    <a href={"home.html?CatID="+item.ID+"&CatName="+item.CatName} className="type_link" data-id={item.ID} data-typename={item.CatName}>{item.CatName+" ("+item.ArticleCount+")"}</a>
                </li>
            )
        });
        return (
            <ul className="type_list">
                {submenus}
            </ul>
        )
    }
}