/*左侧分类目录／文章归档菜单*/
import React, { Component } from 'react';
import Title from './title';

export default class DateList extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { title, icon } = this.props;
        return (
            <div className="type">
                <Title title={title} icon={icon} />
                <ul className="type_list">
                    <li className="type_item">
                        <a href="#" className="type_link">2015年一月</a>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">2016年二月</a>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">2017年三月</a>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">2018年四月</a>
                    </li>
                </ul>
            </div>
        )
    }
}