/*左侧最新文章／最热文章列表*/
import React, {Component} from 'react';
import Title from './title';
import LazyLoad from 'react-lazy-load';

export default class ArticleList extends Component {
    render() {
        const {title, icon, data, type} = this.props;
        const articles = data.map((item, index) => {
            return (
                <li className="article_item" key={index}>
                    <a href={"detail.html?ID=" + item.ID} className="article_link">
                        <LazyLoad>
                            <img
                                src={item.MainImage?('http://7xj89g.com1.z0.glb.clouddn.com/'+item.MainImage):'http://demo.shejidaren.com/dr-ii/wp-content/themes/dr-ii/static/images/min/frontend/thumb-preview.png'}
                                className="article_img"/>
                        </LazyLoad>
                    </a>
                    <div className="article_info">
                        <a href={"detail.html?ID=" + item.ID} className="article_title article_link">{item.Title}</a>
                        <div className="article_other">
                            <a href={"home.html?CatID="+item.CatID+"&CatName="+item.CatName} className="article_type">{item.CatName}</a>
                            {type === "hot"
                                ? (
                                    <span className="article_click">
                                        <i className="iconfont icon-liulan"></i>{item.Clicks}</span>
                                )
                                : ""}
                        </div>
                    </div>

                </li>
            )
        })
        return (
            <div className="article">
                <Title title={title} icon={icon}/>
                <ul>
                    {articles}
                    {/* <li className="article_item">
                        <a href="#" className="article_link">
                            <img src="http://demo.shejidaren.com/dr-ii/wp-content/uploads/2015/01/pp-3-210x140.jpg" className="article_img" />
                            <div className="article_info">
                                <div className="article_title">我是文章标题文章标题文章标题文章标题文章标题文章标题文章标题</div>
                                <a href="#" className="article_type">HTML</a>
                            </div>
                        </a>
                    </li>*/}
                </ul>
            </div>
        )
    }
}