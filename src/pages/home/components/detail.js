/*文章详情*/
import React, { Component } from 'react';

export default class Detail extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { data } = this.props,
            article = data.article,
            prev_article = data.prev,
            next_article = data.next;
        const tags = article.Keywords && article.Keywords.split('、').map((item, index) => {
            return (
                <li className="article_tag" key={index}>{item}</li>
            )
        });
        return (
            <div className="detail">
                <h1 className="detail_name">{article.Title}</h1>
                <div className="detail_tags">
                    <a href={"home.html?CatID="+article.CatID+"&CatName="+data.CatName} className="article_type">{data.catName}</a>
                    <span className="detail_tag"><i className="iconfont icon-dateto"></i><span>{article.CreateDateTime&&article.CreateDateTime.substr(0, 10)}</span></span>
                    <span className="detail_tag"><i className="iconfont icon-liulan"></i><span>{article.Clicks}</span></span>
                    <span className="detail_tag"><i className="iconfont icon-xinxi"></i><span>18</span></span>
                </div>
                <div className="detail_content" dangerouslySetInnerHTML={{__html:article.Content}}></div>
                <div className="article_tags">
                    <span className="description">文章标签</span>
                    <ul> {tags}</ul>
                </div>
                <div className="reprinted_article">
                    {/*百度分享插件*/}
                    <div className="bdsharebuttonbox">
                        <span className="description">转帖：</span>
                        <a href="#" className="bds_more" data-cmd="more"></a>
                        <a href="#" className="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                        <a href="#" className="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
                        <a href="#" className="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a>
                        <a href="#" className="bds_renren" data-cmd="renren" title="分享到人人网"></a>
                        <a href="#" className="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
                    </div>
                    <ul className="reprinted_tips">
                        <li>永久地址：http://y.dobit.top</li>
                        <li>欢迎转载：转载请标明原文出处哦。如有侵权，请及时告知删除。</li>
                    </ul>
                </div>
                <div className="switch_article">
                    {prev_article ? (<a href={"?id=" + prev_article.ID}><span className="prev_article">上一篇</span><span className="article_title">{prev_article.Title}</span></a>) : ''}
                    {next_article ? (<a href={"?id=" + next_article.ID}><span className="next_article">下一篇</span><span className="article_title">{next_article.Title}</span></a>) : ''}
                </div>
                <div className="article_comment">
                    <div id="SOHUCS" sid={article.ID}></div>
                </div>
            </div>
        )
    }
}