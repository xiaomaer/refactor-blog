/*左侧分类目录／文章归档菜单*/
import React, { Component } from 'react';
import Title from './title';
import SubMenus from './subMenus';

export default class TypeList extends Component {
    constructor(props) {
        super(props);
        this.typeArticles = this.typeArticles.bind(this);
    }
    typeArticles(e) {
        const target = e.target;
        if (target.tagName.toLowerCase() === "a") {
            const ID = parseInt(target.dataset['id']),
                typeName = target.dataset['typename'];
            const { typeArticles } = this.props;
            typeArticles(ID,"",typeName);
        }
    }
    render() {
        const { title, icon, types } = this.props;
        const menus = types.map((item, index) => {
            if (item.subTypes) {
                return (
                    <li className="type_item" key={"first_" + index}>
                        <a href={"home.html?CatID="+item.ID+"&CatName="+item.CatName} className="type_link" data-id={item.ID} data-typename={item.CatName}>{item.CatName+" ("+item.ArticleCount+")"}</a>
                        <SubMenus data={item.subTypes} />
                    </li>
                )
            } else {
                return (
                    <li className="type_item" key={"first_" + index}>
                        <a href={"?CatID="+item.ID+"&CatName="+item.CatName} className="type_link" data-id={item.ID} data-typename={item.CatName}>{item.CatName+" ("+item.ArticleCount+")"}</a>
                    </li>
                )
            }
        })
        return (
            <div className="type">
                <Title title={title} icon={icon} />
                <ul className="type_list" onClick={this.typeArticles}>
                    {/*<li className="type_item">
                        <a href="#" className="type_link">前端开发</a>
                        <ul className="subtype_list">
                            <li className="subtype_item">
                                <a href="#" className="type_link">HTML</a>
                            </li>
                            <li className="subtype_item">
                                <a href="#" className="type_link">CSS</a>
                            </li>
                            <li className="subtype_item">
                                <a href="#" className="type_link">JAVASCRIPT</a>
                            </li>
                        </ul>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">后端开发</a>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">移动开发</a>
                    </li>
                    <li className="type_item">
                        <a href="#" className="type_link">其他</a>
                    </li>*/}
                    {menus}
                </ul>
            </div>
        )
    }
}