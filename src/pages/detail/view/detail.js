import React, {Component} from 'react';
import {Container} from 'flux/utils';
import homeStore from '../../home/store/HomeStore';
import Nav from '../../home/components/nav';
import TypeList from '../../home/components/typeList';
import DateList from '../../home/components/dateList';
import ArticleList from '../../home/components/articleList';
import Tags from '../../home/components/tags';
import Detail from '../../home/components/detail';
import API from '../../home/api/homeAPI';

class ArticleDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: homeStore.getState(),
            cid: 0,
            query: "",
            type_name: '全部文章'
        }
        this.bindFunctions();
    }
    static getStores() {
        return [homeStore]
    }
    static calculateState() {
        return {
            info: homeStore.getState()
        }
    }
    bindFunctions() {
        this.typeArticles = this
            .typeArticles
            .bind(this);
    }
    // 根据类型获取文章列表
    typeArticles(typeID, queryStr, typeName) {
        this.setState({
            page_num: 1,
            cid: typeID,
            query: queryStr,
            type_name: typeName
        }, () => {
            API.getArticleList(typeID, queryStr, 1, typeName)
        });
    }

    render() {
        const {info} = this.state;
        return (
            <div>
                <Nav types={info.bigTypes}/>
                <div className="header_fix"></div>
                <div className="banner">
                    <span className="banner_logo page_container">只希望每次的记录都有些许突破～～</span>
                </div>
                <div className="content page_container">
                    <div className="side_left">
                        {/* 文章类型分类*/}
                        <TypeList title="分类目录" types={info.bigTypes} typeArticles={this.typeArticles}/> {/* 文章发布时间分类*/}
                        {/*<DateList title="文章归档"/>*/}
                    </div>
                    <div className="main">
                        {/*文章详情*/}
                        <Detail data={info.articleDetail}/>
                    </div>
                    <div className="side_right">
                        <ArticleList
                            title="最新文章"
                            icon="icon-zuixin1"
                            data={info.newArticles}
                            type="new"/>
                        <ArticleList
                            title="最热文章"
                            icon="icon-remen-copy"
                            data={info.hotArticles}
                            type="hot"/> 
                            {/* <Tags title="标签" icon="icon-biaoqian"/>*/}
                    </div>
                </div>
            </div>
        );
    }
}
export default Container.create(ArticleDetail);