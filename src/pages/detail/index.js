/**
 * Created by xiaoma on 2016/12/20.
 */
import React, {Component} from 'react';
import {render} from 'react-dom';
import Page from './view/detail';
import API from '../home/api/homeAPI';
render(
    <Page/>, document.querySelector('#J-container'));
//获取文章类型列表
API.getArticleAllTypes(2);
//获取最热文章列表
API.getHotArticleList();
//获取文章详情
const ID = parseInt(getUrlParams('ID'));
API.getArticleDetail(ID);
// 获取查询字符串
function getUrlParams(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"),
        r = window
            .location
            .search
            .substr(1)
            .match(reg);
    if (r != null) 
        return decodeURIComponent(r[2]);
    return null;
}